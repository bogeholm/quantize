#! /bin/bash

# Check for changes - https://stackoverflow.com/questions/5143795
if [[ $(git status --porcelain) ]]; then
	# Changes
	git add "csv/*"
	git add "html/*"
	git commit -m "[auto] ($(whoami)) on $(LC_ALL=en_US date +'%A %B %d, %Y, %H:%M:%S')"
	git pull --rebase origin master
	git push origin master
fi