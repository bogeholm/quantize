#! /usr/bin/env python3

import numpy as np
import os
import pandas as pd
import requests
import sys
import unicodedata as ud

from bs4 import BeautifulSoup
from collections import OrderedDict
from datetime import datetime
from datetime import date as dtdate

# TODO: Check HTML status code
# TODO: Typed arguments


def get_kurser(URL="https://www.jyskebank.dk/bolig/boliglaan/kurser"):
    """ Download the mortgage values from Jyske Bank
    """
    return requests.get(URL)


def clean_string(s: str):
    """ Convert string to UTF-8 and remove various escape characters
    """
    return (
        ud.normalize("NFKD", s)
        .strip()
        .replace("\n", "")
        .replace("\b", "")
        .replace("  ", " ")
    )


def clean_array(array):
    """ Clean up an array of strings
    """
    return [clean_string(s) for s in array]


def get_timestamp():
    """ Return an ISO timestamp without microseconds
    """
    return datetime.now().replace(microsecond=0).isoformat().replace(":", "_")


def save_html(req, filename, path="html/"):
    """ Save web request as HTML
    """
    with open(path + filename + ".html", "w+") as f:
        f.write(req.text)


def get_today():
    """ Return an ISO timestamp without microseconds
    """
    today = dtdate.today()
    return "{}-{}-{}".format(today.year, today.month, today.day)


def save_df(df, filename, path):
    """ Save DataFrame into directory <year>-<month>-<day>,
        creating directory if it does not exist
    """
    today = get_today()
    directory = path + today + "/"

    # Ignoring race condition advice in https://stackoverflow.com/questions/273192
    if not os.path.exists(directory):
        os.makedirs(directory)

    df.to_csv(directory + filename + ".csv", sep=";")


def comma2dot(s):
    """ Convert comma to dot: '96,76' -> '96.76'
    """
    return s.replace(",", ".")


comma2dot = np.vectorize(comma2dot)


def extract_data(req):
    """ Extract table data from web request
    """
    soup = BeautifulSoup(req.content, features="html.parser")  # , "lxml")
    table = soup.find("table")

    # Headers
    headers = [th.get_text() for th in table.find("tr").find_all("th")]
    headers = np.array(clean_array(headers))
    if len(headers) > 0:
        headers[0] = "Type"

    # Data
    datasets = []
    for row in table.find_all("tr")[1:]:
        dataset = [td.get_text() for td in row.find_all("td")]
        dataset = clean_array(dataset)
        datasets.append(dataset)

    datasets = np.array(datasets)
    return headers, datasets


def to_df(headers, data):
    """ Convert headers and data to pandas dataframe
    """
    if len(headers) != np.shape(data)[1]:
        errs = "len(headers) ({}) != np.shape(data)[1] ({})".format(
            len(headers), np.shape(data)[1]
        )
        raise ValueError(errs)

    valdict = OrderedDict()

    # Array from dataset
    rownames = data[:, 0]
    values = data[:, 1:]
    values = comma2dot(values).astype(np.float64)

    # Construct dict
    valdict[headers[0]] = rownames

    for (idx, header) in enumerate(headers[1:]):
        valdict[header] = values[:, idx]

    # Convert to DataFrame
    df = pd.DataFrame(valdict)
    df = df.set_index("Type")

    return df


if __name__ == "__main__":
    file_dir = os.path.dirname(os.path.realpath(__file__))
    csv_path = file_dir + "/csv/"
    html_path = file_dir + "/html/"

    request = get_kurser()
    timestamp = get_timestamp()
    headers, data = extract_data(request)

    # If no headers, assume an error, save HTML for analysis
    if len(headers) == 0:
        save_html(request, timestamp, path=html_path)
        sys.exit(1)

    # If creating DataFrame fails, error; save HTML as well
    try:
        df = to_df(headers, data)
    except Exception as ex:
        save_html(request, timestamp, path=html_path)
        print(ex)
        sys.exit(1)

    save_df(df, timestamp, path=csv_path)
