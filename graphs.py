#! /usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import re

from collections import OrderedDict
from datetime import datetime, timedelta

matplotlib.style.use("ggplot")


def list_matching_files(path, regex):
    """ Traverse a directory and return list of filenames matching a regex.
        Returns:
            filenames: list of filenames
            fqfnames: list of filenames with path
    """
    rc = re.compile(regex)
    filenames = []
    fqfnames = []

    for root, _, files in os.walk(path):
        matching = np.array(list(filter(rc.match, files)))
        for file in matching:
            fqfnames.append(os.path.join(root, file))
            filenames.append(file)

    return np.array(filenames), np.array(fqfnames)


# https://thispointer.com/python-how-to-convert-a-timestamp-string-to-a-datetime-object-using-datetime-strptime/
def str2time(instr):
    """ Convert filename in our format to timestamp
    """
    dt_format = "%Y-%m-%dT%H_%M_%S"
    timestr = instr.replace(".csv", "")
    dt = datetime.strptime(timestr, dt_format)
    return dt


def sort_filedir(path, regex):
    """ Accept path and regex
        Find filenames matching a regex representing a timestamp
        Return matching files and timestamps, sorted
    """
    filenames, fqfnames = list_matching_files(path, regex)
    timestamps = np.array([str2time(f) for f in filenames])
    sorter = np.argsort(timestamps)

    return fqfnames[sorter], timestamps[sorter]


def csv_col2df(filename, timestamp, colname):
    """ Create data df from csv file
    """
    # Intermediate DataFrame
    idf = pd.read_csv(filename, sep=";")
    new_cols = idf["Type"].values
    newvals = idf[colname].values

    odict = OrderedDict()
    for (colname, value) in zip(new_cols, newvals):
        odict[colname] = value

    # Index must be 'a collection of some kind'
    return pd.DataFrame(odict, index=[timestamp])


def csvfiles_col2df(colname, path, regex):
    """ Create DataFrame recursively from <regex>.csv files in directory,
        based on column name in *.csv file
    """
    fqfilenames, timestamps = sort_filedir(path, regex)

    dataframes = []
    for (matchfile, timestamp) in zip(fqfilenames, timestamps):
        df = csv_col2df(matchfile, timestamp, colname)
        dataframes.append(df)

    resdf = dataframes[0]
    resdf = resdf.append(dataframes[1:], sort=False)

    return resdf


def csv_row2df(filename, timestamp, rowname):
    """ Create data df from csv file
    """
    df = pd.read_csv(filename, sep=";")
    df = df.loc[df["Type"] == rowname]
    # Index must be 'a collection of some kind'
    df.index = [timestamp]

    return df


def csvfiles_row2df(rowname, path, regex):
    """ Create DataFrame recursively from <regex>.csv files in directory
        based on row name in *.csv file
    """
    fqfilenames, timestamps = sort_filedir(path, regex)

    dataframes = []
    for (matchfile, timestamp) in zip(fqfilenames, timestamps):
        df = csv_row2df(matchfile, timestamp, rowname)
        dataframes.append(df)

    resdf = dataframes[0]
    resdf = resdf.append(dataframes[1:], sort=False)

    return resdf


def savegraph(fig, ax, path):
    """ Save (fig, ax) at path after some customization
    """
    if ax.get_legend() is not None:
        ax.get_legend().get_frame().set_linewidth(0.0)
        ax.get_legend().get_frame().set_facecolor(fig.get_facecolor())
    # ax.set_ylim(95, 105)
    # https://jakevdp.github.io/PythonDataScienceHandbook/04.10-customizing-ticks.html
    ax.xaxis.set_major_locator(plt.MaxNLocator(8))
    fig.savefig(
        path, facecolor=fig.get_facecolor(), edgecolor="none", bbox_inches="tight"
    )


if __name__ == "__main__":
    cwd = os.path.dirname(os.path.realpath("__file__"))
    csvpath = cwd + "/csv/"

    # Regular expression for filenames we will accept
    regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.csv"
    date_formatter = "%Y-%m-%d %H:%M:%S"

    # Column names in raw data
    aktuel = "Aktuel kurs"
    tilbud = "Tilbudskurs"
    fastkurs_30_12_2019 = "Udbetalingskurs Fastkursaftale til 30.12.2019"
    # Row names in raw data
    thirty_half = "30 år 0,50 %"
    thirty_one = "30 år 1,00 %"
    fix_cols = [
        "Tilbudskurs",
        "Aktuel kurs",
        "Udbetalingskurs Fastkursaftale til 30.12.2019",
    ]

    half_df = csvfiles_row2df(thirty_half, csvpath, regex)
    one_df = csvfiles_row2df(thirty_one, csvpath, regex)
    half_df = half_df[fix_cols]
    one_df = one_df[fix_cols]

    # '30 år 0,50 %'
    half_fig, half_ax = plt.subplots(1, 1)
    half_df.plot(ax=half_ax)
    half_ax.get_legend().set_bbox_to_anchor((0.775, 1.25))
    savegraph(half_fig, half_ax, "gfx/half_thirty.svg")

    # '30 år 1,00 %'
    one_fig, one_ax = plt.subplots(1, 1)
    one_df.plot(ax=one_ax)
    one_ax.get_legend().set_bbox_to_anchor((0.775, 1.25))
    savegraph(one_fig, one_ax, "gfx/one_thirty.svg")

    # 'Tilbudskurs'
    tilbud_df = csvfiles_col2df(tilbud, csvpath, regex)

    # Same columns in other DatFrames
    new_cols = [s for s in tilbud_df.columns.values if "afdrag" not in s]
    new_cols = [s for s in new_cols if "30" in s]
    tilbud_df = tilbud_df[new_cols]

    tilbud_fig, tilbud_ax = plt.subplots(1, 1)
    tilbud_df.plot(ax=tilbud_ax)
    tilbud_ax.get_legend().set_bbox_to_anchor((1, 1))
    savegraph(tilbud_fig, tilbud_ax, "gfx/tilbud.svg")

    # 'Aktuel kurs'
    aktuel_df = csvfiles_col2df(aktuel, csvpath, regex)
    aktuel_df = aktuel_df[new_cols]

    aktuel_fig, aktuel_ax = plt.subplots(1, 1)
    aktuel_df.plot(ax=aktuel_ax)
    aktuel_ax.get_legend().set_bbox_to_anchor((1, 1))
    savegraph(aktuel_fig, aktuel_ax, "gfx/aktuel.svg")

    # 'Fastkursaftale til 30.12.2019'
    fastkurs_df = csvfiles_col2df(fastkurs_30_12_2019, csvpath, regex)
    fastkurs_df = fastkurs_df[new_cols]

    fastkurs_fig, fastkurs_ax = plt.subplots(1, 1)
    fastkurs_df.plot(ax=fastkurs_ax)
    fastkurs_ax.get_legend().set_bbox_to_anchor((1, 1))
    savegraph(fastkurs_fig, fastkurs_ax, "gfx/fastkurs_30_12_2019.svg")

    # '30 år 1,00 %' - previous 48 hours
    present = datetime.now()
    past = present - timedelta(days=2)
    past_str = past.strftime(date_formatter)

    # Check that there's data before plotting
    prev48_df = one_df[one_df.index >= past_str]
    if len(prev48_df > 0):
        prev48_fig, prev48_ax = plt.subplots(1, 1)
        prev48_df.plot(ax=prev48_ax)
        prev48_ax.get_legend().set_bbox_to_anchor((0.775, 1.25))
        savegraph(prev48_fig, prev48_ax, "gfx/prev48.svg")
