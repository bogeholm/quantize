#! /usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import toml

from graphs import csvfiles_row2df, savegraph


def annuity(G, r, n):
    """ Calculate annuity mortgage
    """
    return G * r / (1 - (1 + r) ** (-n))


def single_mortgage(hovedstol, rentesats, bidragssats, terminer=4 * 30, verbose=False):
    """ Lav en df ud fra låneparametre
    """
    ydelse = annuity(hovedstol, rentesats, terminer)

    # Space for results
    rest_vec = np.zeros(terminer + 1)
    termin_vec = np.zeros(terminer + 1)
    afdrag_vec = np.zeros(terminer + 1)
    rente_vec = np.zeros(terminer + 1)
    bidrag_vec = np.zeros(terminer + 1)

    # Start the mortgage
    rest_vec[0] = hovedstol

    # Run the mortgage
    for idx in range(1, terminer + 1):
        this_rente = rentesats * rest_vec[idx - 1]
        this_afdrag = ydelse - this_rente
        this_rest = rest_vec[idx - 1] - this_afdrag
        this_bidrag = bidragssats * rest_vec[idx - 1]
        this_termin = ydelse + this_bidrag

        rest_vec[idx] = max(0, np.round(this_rest, 2))
        termin_vec[idx] = np.round(this_termin, 2)
        afdrag_vec[idx] = np.round(this_afdrag, 2)
        rente_vec[idx] = np.round(this_rente, 2)
        bidrag_vec[idx] = np.round(this_bidrag, 2)

    df = pd.DataFrame(
        {
            "Rest": rest_vec,
            "Termin": termin_vec,
            "Afdrag": afdrag_vec,
            "Rente": rente_vec,
            "Bidrag": bidrag_vec,
        }
    )

    # Sanity check of values
    money_conservation = (
        df["Termin"] - df["Rente"] - df["Bidrag"] - df["Afdrag"]
    ).values
    money_err = "Money is not conserved: (Termin - Bidrag - Rente) != zero: {}".format(
        money_conservation
    )
    # We'll accept a rounding error less than 2 deci-money (cents, øre, ...)
    if np.max(np.abs(money_conservation)) >= 0.02:
        raise FloatingPointError(money_err)

    if verbose:
        print("Max rounding error: {}".format(np.round(np.max(money_conservation), 3)))

    return df


def mortgage_series(series, interest_percent, **kwargs):
    """ From a pandas series of mortage prices, calculate mortgage indicators
    """
    # Check kwargs
    for kwarg in ["principal", "valuation", "contrib_percent", "payments"]:
        if kwarg not in kwargs:
            errs = "'{}' not passed in kwargs".format(kwarg)
            raise ValueError(errs)
    # Get kwargs
    principal = kwargs["principal"]
    valuation = kwargs["valuation"]
    contrib_percent = kwargs["contrib_percent"]
    payments = kwargs["payments"]
    # Form percent to fraction
    contrib = contrib_percent / 100
    interest = interest_percent / 100

    # Series content
    prices = series.values
    index = series.index
    # Run calculation only one for each unique price
    uniques = np.sort(np.unique(prices))
    # For convenience
    zero = np.zeros(len(uniques))
    # DataFrame of indicators for each price
    pricemap = pd.DataFrame(
        {"Kurs": uniques, "Total": zero, "1. Termin": zero, "Belåningsgrad": zero}
    )

    # Construct map for unique price values to indicators
    for idx, price in enumerate(pricemap["Kurs"].values):
        # Price is in percent - calculate for the single mortgage
        price_fraction = price / 100
        mdf = single_mortgage(principal / price_fraction, interest, contrib, payments)

        # Total due and 1st down payment
        pricemap.iloc[idx]["Total"] = np.round(mdf["Termin"].sum(), 0)
        pricemap.iloc[idx]["1. Termin"] = np.round(mdf["Termin"].values[1], 0)
        # Leverage
        leverage = np.round((principal / price_fraction) / valuation * 100, 2)
        pricemap.iloc[idx]["Belåningsgrad"] = leverage

    # Construct df to return
    resdf = pd.DataFrame({"Kurs": prices})
    resdf.index = index

    # Total due
    total_dict = pricemap.set_index("Kurs").to_dict()["Total"]
    total_series = np.array(list(map(np.vectorize(total_dict.get), prices)))
    resdf["Total"] = total_series

    # 1st down payment
    payment_dict = pricemap.set_index("Kurs").to_dict()["1. Termin"]
    payment_series = np.array(list(map(np.vectorize(payment_dict.get), prices)))
    resdf["1. Termin"] = payment_series

    # Leverage
    leverage_dict = pricemap.set_index("Kurs").to_dict()["Belåningsgrad"]
    leverage_series = np.array(list(map(np.vectorize(leverage_dict.get), prices)))
    resdf["Belåningsgrad"] = leverage_series

    return resdf


if __name__ == "__main__":
    # Variable names - should be defined globally
    cwd = os.path.dirname(os.path.realpath("__file__"))
    csvpath = cwd + "/csv/"
    print(csvpath)

    # Regular expression for filenames we will accept
    regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.csv"

    fastkurs_30_12_2019 = "Udbetalingskurs Fastkursaftale til 30.12.2019"
    thirty_half = "30 år 0,50 %"
    thirty_one = "30 år 1,00 %"

    # Get example values
    with open("website/data/credit.toml", "r") as tf:
        cvals = toml.load(tf)
    realvals = cvals["example"]

    df05_data = csvfiles_row2df(thirty_half, csvpath, regex)
    df10_data = csvfiles_row2df(thirty_one, csvpath, regex)

    df05_series = mortgage_series(df05_data[fastkurs_30_12_2019], 0.5 / 4, **realvals)
    df10_series = mortgage_series(df10_data[fastkurs_30_12_2019], 1.0 / 4, **realvals)

    # Total amount due
    total_fig, total_ax = plt.subplots(1, 1)

    df05_series["Total"].plot(ax=total_ax, label=thirty_half)
    df10_series["Total"].plot(ax=total_ax, label=thirty_one)
    total_ax.legend()
    total_ax.get_legend().set_bbox_to_anchor((1, 1))

    total_ax.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ","))
    )

    savegraph(total_fig, total_ax, "gfx/total_fastkurs_30_12_2019.svg")

    # 1st down payment
    payment_fig, payment_ax = plt.subplots(1, 1)

    df05_series["1. Termin"].plot(ax=payment_ax, label=thirty_half)
    df10_series["1. Termin"].plot(ax=payment_ax, label=thirty_one)
    payment_ax.legend()
    payment_ax.get_legend().set_bbox_to_anchor((1, 1))

    payment_ax.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ","))
    )

    savegraph(payment_fig, payment_ax, "gfx/termin_fastkurs_30_12_2019.svg")

    # Leverage
    leverage_fig, leverage_ax = plt.subplots(1, 1)

    df05_series["Belåningsgrad"].plot(ax=leverage_ax, label=thirty_half)
    df10_series["Belåningsgrad"].plot(ax=leverage_ax, label=thirty_one)
    leverage_ax.legend()
    leverage_ax.get_legend().set_bbox_to_anchor((1, 1))

    savegraph(leverage_fig, leverage_ax, "gfx/belaaning_fastkurs_30_12_2019.svg")

    # Save data for inspection
    df05_series.to_csv("data/df05_series.csv")
    df10_series.to_csv("data/df10_series.csv")
