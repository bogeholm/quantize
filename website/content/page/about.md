### About
This is a personal hobby project started to learn more about the [Realkredit](https://www.google.com/search?q=realkredit). 

The software powering this page, and the page itself, is released under the [MIT License](https://gitlab.com/bogeholm/quantize/blob/master/LICENSE). Please read that carefully to understand that this page, and the information it contains, comes with *no warranty*. Thou hast been warned ;-)

For minimal background visit [bogeholm.org](https://www.bogeholm.org).