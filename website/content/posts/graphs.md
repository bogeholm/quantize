---
title: "Kurser"
date: 2019-10-15T17:08:30+02:00
draft: false
---

Last updated at <mark>{{< gfxmod >}}<mark>.

### 0.5% fastforrentet 30 år
![0.5%](/gfx/half_thirty.svg)

### 1% fastforrentet 30 år
![1%](/gfx/one_thirty.svg)

### Tilbudskurs
![Tilbudskurs](/gfx/tilbud.svg)

### Aktuel kurs
![Aktuel kurs](/gfx/aktuel.svg)

### Fastkursaftale til 30-12-2019
![Fastkurs](gfx/fastkurs_30_12_2019.svg)
