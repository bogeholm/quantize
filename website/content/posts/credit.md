---
title: "Krediteksempel"
date: 2019-10-15T17:00:31+02:00
draft: false
---

Løbende eksempel, baseret på 'Fastkursaftale til 30-12-2019' fra [Kurser]({{< ref "graphs.md" >}}). Sidst opdateret <mark>{{< gfxmod >}}<mark>.

### Total tilbagebetaling
![Total amount due](/gfx/total_fastkurs_30_12_2019.svg)

### Første termin
![1st payment](/gfx/termin_fastkurs_30_12_2019.svg)

### Belåningsgrad
![Leverage](/gfx/belaaning_fastkurs_30_12_2019.svg)

### Beregningsgrundlag
{{< credit-table >}}