#! /bin/sh
datafile="website/data/gfxmod.toml"

# https://devhints.io/git-log-format
timestamp="$(git log -1 --format="%ci" -- csv)"

echo "Writing timestamp to datafile:"
echo "datafile: ${datafile}"
echo "timestamp: ${timestamp}"

echo "lastmod = \"${timestamp}\""  > ${datafile}