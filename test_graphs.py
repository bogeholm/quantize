from datetime import datetime
import os
import numpy as np

from graphs import str2time, list_matching_files

# Regex for input files to match on
regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}_[0-9]{2}_[0-9]{2}.csv"

# Relative path to input test files
csv_input_path = "testdata/test-csv-input"

# List of input directories
csv_input_unique_dirs = [
    "testdata/test-csv-input/2019-10-14",
    "testdata/test-csv-input/2019-10-15",
    "testdata/test-csv-input/2019-10-16",
]

# List of input filelist
csv_input_filelist = [
    "2019-10-16T12_00_07.csv",
    "2019-10-16T18_00_06.csv",
    "2019-10-16T15_00_05.csv",
    "2019-10-16T09_00_07.csv",
    "2019-10-14T09_00_06.csv",
    "2019-10-14T18_00_05.csv",
    "2019-10-14T15_00_07.csv",
    "2019-10-14T12_00_06.csv",
    "2019-10-15T12_00_08.csv",
    "2019-10-15T09_00_06.csv",
    "2019-10-15T18_00_05.csv",
    "2019-10-15T15_00_06.csv",
]


def test_str2time():
    datetime_str = "08/03/19 03:30:00"
    datetime_object = datetime.strptime(datetime_str, "%d/%m/%y %H:%M:%S")

    str2time_str = "2019-03-08T03_30_00"
    str2time_object = str2time(str2time_str)

    assert datetime_object == str2time_object


def test_filelist_list_matching_files():
    filelist_test_res, _ = list_matching_files(csv_input_path, regex)
    # Check we found all files
    for fname in csv_input_filelist:
        assert fname in filelist_test_res

    # Check we only found what we expect
    for fname in filelist_test_res:
        assert fname in csv_input_filelist


def test_relpath_list_matching_files():
    _, filelist_fqnames = list_matching_files(csv_input_path, regex)
    dirs_test_res = [os.path.dirname(fqname) for fqname in filelist_fqnames]
    unique_dirs_res = np.unique(dirs_test_res)

    for dirname in csv_input_unique_dirs:
        assert dirname in unique_dirs_res

    for dirname in unique_dirs_res:
        assert dirname in csv_input_unique_dirs
