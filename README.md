# Quantize
Time series of mortgage prices from [Jyske Bank](https://www.jyskebank.dk/bolig/boliglaan/kurser).

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Build status](https://gitlab.com/bogeholm/quantize/badges/master/pipeline.svg)](https://gitlab.com/bogeholm/quantize/pipelines/)
[![Coverage](https://gitlab.com/bogeholm/quantize/badges/master/coverage.svg)](https://codecov.io/gl/bogeholm/quantize)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/bogeholm/quantize/blob/master/LICENSE) 

# Executive summary
* Download mortgage prices from [Jyske Bank](https://www.jyskebank.dk/bolig/boliglaan/kurser)
* Store the data and convert to timeseries
* Build website with results at [bogeholm.gitlab.io/quantize/](https://bogeholm.gitlab.io/quantize/)

# Documentation
[`prices.py`](https://gitlab.com/bogeholm/quantize/blob/master/prices.py) downloads the prices and stores results in a distributed, version-controlled *JBOFF*-database ([Just a Bunch Of Files and Folders](https://gitlab.com/bogeholm/quantize/tree/master/csv)). Database communication is handled by [`cloudsync.sh`](https://gitlab.com/bogeholm/quantize/blob/master/cloudsync.sh), and [`graphs.py`](https://gitlab.com/bogeholm/quantize/blob/master/graphs.py) builds the time series. Finally a credit example is calculated in [`credit.py`](https://gitlab.com/bogeholm/quantize/blob/master/credit.py).

### Raspberry Pi part
These [`crontab`](https://crontab.guru/) entries on the Pi will download data each hour at zero minutes past, and upload data to the JBOFF cloud at 2 minutes past:
```bash
# $ crontab -e
# m h dom mon dow command
# Make sure to point to the correct python version
0  *  *  *  * /home/pi/berryconda3/bin/python3 /home/pi/Code/quantize/prices.py
2  *  *  *  * cd /home/pi/Code/quantize && ./cloudsync.sh
# (vim) :wq
```
Or maybe [every 30 minutes](https://stackoverflow.com/questions/16094545/how-can-i-run-a-cron-job-every-5-minutes-starting-from-a-time-other-than-0-minut):
```bash
*/30    *  *  *  * /home/pi/berryconda3/bin/python3 /home/pi/Code/quantize/prices.py
*/30+2  *  *  *  * cd /home/pi/Code/quantize && ./cloudsync.sh
```


### GitLab CI part
Graphs and website are built from the [`.gitlab-ci.yml`](https://gitlab.com/bogeholm/quantize/blob/master/.gitlab-ci.yml).


# Built with ❤️ and:
### Infrastructure
* [Raspberry Pi](https://www.raspberrypi.org/)
* [GitLab CI](https://about.gitlab.com/product/continuous-integration/)

### Data Analysis
* [Python](https://www.python.org/) with [Berry Conda](https://github.com/jjhelmus/berryconda)
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [NumPy](https://numpy.org/)
* [Pandas](https://pandas.pydata.org/)
* [Matplotlib](https://matplotlib.org/)

### Website
* [Hugo](https://gohugo.io/)
* [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)

# License
[MIT](https://gitlab.com/bogeholm/quantize/blob/master/LICENSE)
